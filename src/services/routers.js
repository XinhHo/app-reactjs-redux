import React, {useState} from "react";
import { BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import AddItem from '../component/add-item/add-item';
import Navbar from '../component/cart-product/navbar/navbar';
import Products from '../component/cart-product/products/products';
import Cart from '../component/cart-product/cart/cart';
import Admin from '../component/admin-page/admin/admin';
import Login from '../component/admin-page/login/login';
import Logout from '../component/admin-page/logout/logout';
import Employees from '../component/employee/';

function Routers(params) {
    return (
        [
            {
                path: "/",
                component: Admin,
                exact: true,
                name: "dashboard",
                // "icon": "dashboard",
            },
            {
                path: "/products",
                component: Products,
                exact: true,
                name: "Products",
                // "icon": "dashboard",
            },
            {
                path: "/employees",
                component: Employees,
                exact: true,
                name: "Employees",
                // "icon": "dashboard",
            },

        ]

    )
    
}
export default Routers;