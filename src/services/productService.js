export function getProducts() {
    return fetch('https://600fc9d56c21e1001704f3aa.mockapi.io/products/products')
      .then(data => data.json())
}
export function getProductById(id) {
  return fetch(`https://600fc9d56c21e1001704f3aa.mockapi.io/products/products/${id}`)
    .then(data => data.json())
}
export function addNewProduct(product) {
    return fetch('https://600fc9d56c21e1001704f3aa.mockapi.io/products/products', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ 
        productName: product.productName,
        priceDeault: product.priceDeault,
        amount: product.amount,
        image: product.image,
        expiredDate: product.expiredDate,
        description: product.description
       })
    })
      .then(data => data.json())
}
export function updateProduct(id, product) {
  const test = JSON.stringify({ 
    productName: product.productName,
    priceDeault: product.priceDeault,
    amount: product.amount,
    image: product.image,
    expiredDate: product.expiredDate,
    description: product.description
});
   return fetch(`https://600fc9d56c21e1001704f3aa.mockapi.io/products/products/${id}`, {
     method: 'PUT',
     headers: {
       'Content-Type': 'application/json'
     },
     body: test
   })
     .then(data => data.json())
}