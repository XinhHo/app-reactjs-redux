export function getListUser() {
    return fetch('http://localhost:3000/student')
      .then(data => data.json())
}
export function addNewUser(user) {
   console.log('user', user);
    return fetch('http://localhost:3000/student', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ user })
    })
      .then(data => data.json())
}