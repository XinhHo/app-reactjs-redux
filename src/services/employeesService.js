export function getListEmployee() {
    return fetch('https://600fc9d56c21e1001704f3aa.mockapi.io/products/Employees')
      .then(data => data.json())
}
export function getEmployeeById(id) {
  return fetch(`https://600fc9d56c21e1001704f3aa.mockapi.io/products/Employees/${id}`)
    .then(data => data.json())
}
export function addNewEmployee(employee) {
   console.log('user', employee);
    return fetch('https://600fc9d56c21e1001704f3aa.mockapi.io/products/Employees', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ 
        firstName: employee.firstName,
        lastName: employee.lastName,
        level: employee.level,
        department: employee.department,
        gender:employee.gender,
        avatar:employee.avatar,
        onboardStatus: employee.onboardStatus,
        birthDate: employee.birthDate,
        address: employee.address,
        onboardDate: employee.onboardDate
       })
    })
      .then(data => data.json())
}
export function updateEmployee(id, data) {
   return fetch(`https://600fc9d56c21e1001704f3aa.mockapi.io/products/Employees/${id}`, {
     method: 'PUT',
     headers: {
       'Content-Type': 'application/json'
     },
     body: JSON.stringify({ 
       firstName: data.firstName,
      lastName: data.lastName,
      level: data.level,
      department: data.department,
      gender:data.gender,
      avatar:data.avatar,
      onboardStatus: data.onboardStatus,
      birthDate: data.birthDate,
      address: data.address,
      onboardDate: data.onboardDate
    })
   })
     .then(data => data.json())
}