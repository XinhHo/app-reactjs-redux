import './search.scss';
import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import 'antd/dist/antd.css';
import {DatePicker, Form, Input, Select, Radio, Button } from 'antd';
function Search(props) {
    let history = useHistory();
    const [gender, setGender] = useState('');
    const [name, setNameSearch] = useState('');
    const [onboardStatus, setOnboardStatus] = useState('');
    const [birthDate, setBirthDate] = useState('');
    const [level, setLevel] = useState('');
    const radioStyle = {
        display: 'block',
        height: '30px',
        lineHeight: '30px',
    };
    const { Option } = Select;
    const onChangeGender = (e) => {
        setGender(e.target.value);
    };
    const onChangeName = (e) => {
        setNameSearch(e.target.value);
    };
    const onChangeOnboardStatus = (e) => {
        setOnboardStatus(e);
    };
    const onChangeBirthDate = (date, dateString) => {
        setBirthDate(dateString);
    };
    const onChangeLevel = (e) => {
        setLevel(e);
    };
    const handleSearch = () => {
        props.muiltiSearch({
            name: name,
            onboardStatus: onboardStatus,
            birthDate: birthDate,
            gender: gender,
            level: level
        });
    }
    const addNewEmployee = () => {
        history.push("/employee-detail");
    };
    return(
        <div className="search-content">
            <div className="search-column">
                <div className="search-comp-name">
                    <label>Name</label>
                    <Input placeholder="Please input .... " value={name} onChange={(e) => onChangeName(e)}/>
                </div>
                <div className="search-comp-status">
                    <label>Onboard Status</label>
                    <Select showSearch
                        style={{ width: 200 }}
                        placeholder="Search to Select"
                        value={onboardStatus} 
                        onChange={(e) => onChangeOnboardStatus(e)}
                    > 
                        <Option value="">None</Option>
                        <Option value="work">Work</Option>
                        <Option value="leave">Leave</Option>

                    </Select>
                </div>
                <div className="search-comp-birthDate">
                    <label>BirthDate</label>
                    <DatePicker format={"DD/MM/YYYY"} onChange={ onChangeBirthDate }/>
                </div>
                <div className="search-comp-gender">
                    <label>Gender</label>
                    <Radio.Group onChange={(e) => onChangeGender(e)} value={gender} className="gender">
                        <Radio style={radioStyle} value={" male"}>Male</Radio>
                        <Radio style={radioStyle} value={"female"}>Female</Radio>
                        <Radio style={radioStyle} value={"other"}>Different</Radio>
                    </Radio.Group>
                </div>
                <div className="search-comp-level">
                    <label>Level</label>
                    <Select 
                        style={{ width: 200 }}
                        placeholder="Search to Select"
                        optionFilterProp="children"
                        onChange={(e) => onChangeLevel(e)} 
                        value={level}
                    > 
                        <Option value="">None</Option>
                        <Option value="level 1">Level 1</Option>
                        <Option value="level 2">Level 2</Option>
                        <Option value="level 3">Level 3</Option>
                        <Option value="level 4">Level 4</Option>
                        <Option value="level 5">Level 5</Option>

                    </Select>
                </div>
            </div>
            <div className="add-btn">
                <Button type="primary" onClick={() => handleSearch()}>Search</Button>
                <Button type="primary" onClick={() => addNewEmployee()}>New Employee</Button>
            </div>
            
        </div>
    )
    
}

export default Search;