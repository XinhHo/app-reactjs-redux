import { Modal, Button } from 'antd';
import React, { useState } from 'react';
import './update-modal.scss';
function ModalDefault(props) {
    const { hideModal, SubmitModal, modal } = props;
    const [visible, setVisible] = useState(false);
    const [confirmLoading, setConfirmLoading] = useState(false);
    const [modalText, setModalText] = useState('');
    const [modalTitle, setModalTitle] = useState('');

  if (!modal) {
    return null;
  }

  const handleOk = () => {
    SubmitModal(modal.rowData);
    setTimeout(() => {
        handleCancel()
    }, 2000)
    
  };

  const handleCancel = () => {
    hideModal(false)
  };

  return (
    <>
      <Modal
        title={modal.title}
        visible={modal.visible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <div>{modal.modalText}</div>
      </Modal>
    </>
  )
}
export default ModalDefault;

