import React from "react";
import { DatePicker, Form, Input, TimePicker, Select, Avatar, Radio, Upload,  InputNumber } from "antd";
import capitalizeFirstLetter from '../../share-service/capitalizeFirstLetter';
const FormItem = Form.Item;
const { Option } = Select;
const radioStyle = {
    display: 'block',
    height: '30px',
    lineHeight: '30px',
};

const CreateAntField = AntComponent => ({
  field,
  form,
  hasFeedback,
  label,
  selectOptions,
  selectRadio,
  submitCount,
  type,
  ...props
}) => {
  const touched = form.touched[field.name];
  const submitted = submitCount > 0;
  const hasError = form.errors[field.name];
  const submittedError = hasError && submitted;
  const touchedError = hasError && touched;
  const onInputChange = ({ target: { value } }) =>
    form.setFieldValue(field.name, value);
  const onChange = value => {
    form.setFieldValue(field.name, value)}
    ;
 
  const onBlur = () => form.setFieldTouched(field.name, true);
  return (
    <div className="field-container">
      <FormItem
        label={label}
        hasFeedback={
          (hasFeedback && submitted) || (hasFeedback && touched) ? true : false
        }
        help={submittedError || touchedError ? hasError : false}
         validateStatus={submittedError || touchedError ? "error" : "success"}
      >
        {!selectRadio && 
          <AntComponent
            {...field}
            {...props}
            onBlur={onBlur}
            onChange={type ? onInputChange : onChange}
            >
            {
              selectOptions && selectOptions.map(name => <Option key={name}>{name}</Option>)
            }
            
            </AntComponent>
        }
        {selectRadio && <AntComponent.Group
          {...field}
          {...props}
          onBlur={onBlur}
          onChange={type ? onInputChange : onChange}
          value={field.value.target ? field.value.target.value : field.value}
        >
          {
            selectRadio.map(name => {
              // console.log('field', field);
              // console.log('props', props);
              return <Radio style={radioStyle} key={name} value={name}>{name}</Radio>;
            })
          }
          
        </AntComponent.Group>}
      </FormItem>
    </div>
  );
};

export const AntSelect = CreateAntField(Select);
export const AntDatePicker = CreateAntField(DatePicker);
export const AntInput = CreateAntField(Input);
export const AntTimePicker = CreateAntField(TimePicker);
export const AntAvatar = CreateAntField(Avatar);
export const AntRadio = CreateAntField(Radio);
export const AntUpload = CreateAntField(Upload);
export const AntInputNumber = CreateAntField(InputNumber);
export const AntTextArea = CreateAntField(Input.TextArea);
