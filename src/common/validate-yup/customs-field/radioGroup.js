import React from "react";
import PropTypes from "prop-types";

const RadioGroup = ({ name, options, inputRef, onChangeValue }) => {
  return (
    <div className="radio-opptions">
        {options.map(({ label: optionLabel, value }, index) => {
          return (
            <div key={index} className="radio-item">
              <input
                id={index}
                name={name}
                type="radio"
                value={value}
                ref={inputRef}
                onChange={onChangeValue}
                className="radio-item-input"
              />
              <label htmlFor={index}>
                <span>{optionLabel}</span>
              </label>
            </div>
          );
        })}
      </div>
  );
};

RadioGroup.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired
    })
  )
};
export default RadioGroup;