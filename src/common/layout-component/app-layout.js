import 'antd/dist/antd.css';
import React, {useState, useEffect} from "react";
import { BrowserRouter as Router, Switch, Route, Link, withRouter } from 'react-router-dom';
import { Layout, Menu} from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  BarChartOutlined,
  UnorderedListOutlined,
} from '@ant-design/icons';
import AddItem from '../../component/add-item/add-item';
import Navbar from '../../component/cart-product/navbar/navbar';
import Products from '../../component/cart-product/products/products';
import Cart from '../../component/cart-product/cart/cart';
import Dashboard from '../../component/admin-page/admin/admin';
import Login from '../../component/admin-page/login/login';
import Logout from '../../component/admin-page/logout/logout';
import Employees from '../../component/admin-page/employee/index';
import EmployeesDetail from '../../component/admin-page/employee-detail/employee-detail';
import ProductAdmin from '../../component/admin-page/Products-admin/index';
import ProductDetail from '../../component/admin-page/Products-admin/edit-product/index';
const { Header, Sider, Content } = Layout;
function AppLayout(props) {
  const [currentPath, setcurrentPath] = useState('');
  const [collapsed, setCollapsed] = useState(false);
  const toggle = () => {
    var onclickCollaps = !collapsed;
    setCollapsed(onclickCollaps);
  };
  let pathName = window.location;
  useEffect(() => {
    setcurrentPath(pathName);
    console.log('pathName', pathName);
  }, [pathName]);
 
  return (
    <Router>
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" selectedKeys={[pathName]} defaultSelectedKeys={[pathName]}>
         
           <Menu.Item key="/dashboard" icon={<BarChartOutlined />}>
              <Link to="/dashboard" >
                Dashboard
              </Link>
            </Menu.Item>
            <Menu.Item key="/products" icon={<UnorderedListOutlined />} > 
              <Link to="/products" >
                Products
              </Link>
            </Menu.Item>
            <Menu.Item key="/employee" icon={<UserOutlined />}> 
              <Link to="/employee" >
                Employee
              </Link>
            </Menu.Item>
            {/* <Menu.Item key="4" icon={<UserOutlined />}> 
              <Link to="/employee-detail">
                Employee Detail
              </Link>
            </Menu.Item> */}
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: () => toggle(),
            })}
          </Header>
          <Content className="site-layout-background content">
            <div className="content-comp">
              <Switch>
                <Route exact path="/dashboard" component={Dashboard}></Route>
                <Route exact path="/products" component={ProductAdmin}></Route>
                <Route path="/product-detail/:id" component={ProductDetail}></Route>
                <Route path="/product-detail" component={ProductDetail}></Route>
                <Route exact path="/employee" component={Employees}></Route>
                <Route exact path="/employee-detail" component={EmployeesDetail}></Route>
                <Route path="/employee-detail/:id" component={EmployeesDetail}></Route>
              </Switch>
            </div>
          </Content>
        </Layout>
      </Layout>
    </Router>
  );
}

export default withRouter(AppLayout);