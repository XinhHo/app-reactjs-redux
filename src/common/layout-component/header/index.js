import 'antd/dist/antd.css';
import React, {useState} from "react";
import { BrowserRouter as  withRouter } from 'react-router-dom';
import { Layout } from 'antd';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined
  } from '@ant-design/icons';
  const { Header } = Layout;
function HeaderSite(props) {
    console.log('props-1', props)
    const [collapsed, setCollapsed] = useState(false);
    const toggle = () => {
        props.toggle();
    }
    return (
        <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(props.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: () => toggle(),
            })}
        </Header>
    )

}
export default HeaderSite;