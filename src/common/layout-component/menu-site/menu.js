import 'antd/dist/antd.css';
import './menu.scss';
import React, { useEffect, useState } from "react";
import { Link, withRouter } from 'react-router-dom';
import {
    UserOutlined,
    BarChartOutlined,
    UnorderedListOutlined,
  } from '@ant-design/icons';
  import { Menu, Layout } from 'antd';
  const { Sider } = Layout;


function MenuSite(props) {
    const { collapsed } = props;
    const curentPath = props.location.pathname;
    let pathName = '';
    if (curentPath.toLowerCase().indexOf('product') === 1) {
        pathName = '/products';
    } else if (curentPath.toLowerCase().indexOf('employee') === 1)  {
        pathName = '/employee';
    } else {
        pathName = curentPath;
    }    
    useEffect(() => {
        props.changePathName(pathName);
    }, [pathName]);
    
    
    return (
        <div className="menu-site">
            <Sider trigger={null} collapsible collapsed={collapsed}>
                <div className="logo" />
                <Menu theme="dark" mode="inline" selectedKeys={[pathName]} defaultSelectedKeys={[pathName]}>
                    <Menu.Item key="/dashboard" icon={<BarChartOutlined />}>
                        <Link to="/dashboard" >
                            Dashboard
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="/products" icon={<UnorderedListOutlined />} > 
                        <Link to="/products" >
                            Products
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="/employee" icon={<UserOutlined />}> 
                        <Link to="/employee" >
                            Employee
                        </Link>
                    </Menu.Item>
                {/* <Menu.Item key="4" icon={<UserOutlined />}> 
                <Link to="/employee-detail">
                    Employee Detail
                </Link>
                </Menu.Item> */}
                </Menu>
            </Sider>
        </div>
    )

}
export default withRouter(MenuSite);