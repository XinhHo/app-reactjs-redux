import { combineReducers } from "redux";

import shoppingReducer from './shopping/shopping-reducer';
import adminReducer from './adminReudcer/admin-reducer';

const rootReducer = combineReducers({
  shop: shoppingReducer,
  admin: adminReducer

});

export default rootReducer;