import { ADD_TODO, REMOVE_TODO } from '../actionTypes.js';
const initialState = {
    allIds: [],
    byIds: {}
}
export default function(state = initialState, action) {
    switch (action.type) {
        case ADD_TODO: {
            const { id, content } = action.payload;
            const abc = {
                ...state,
                allIds: [...state.allIds, id],
                byIds: {
                    ...state.byIds,
                    [id]: {
                        content
                    }
                }
            };
            return abc;
        }
        case REMOVE_TODO: {
            debugger;
            const { id } = action.payload;
            const array = [...state.allIds];
            return {
                ...state,
                allIds: array.filter(index => index !== id)
            }

        }
        default:
            return state;
    }
}