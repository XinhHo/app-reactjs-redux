import * as actionTypes from './admin-types';
const INITIAL_STATE = {
    employeeList: [],
    currentEmployee: null,
    searchValue: {
        name: '',
        onboardStatus: '',
        birthDate: '',
        gender: '',
        level: ''
    },
};
const adminReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.GET_EMPLOYEES:
            return {
                ...state,
                employees: action.payload.employeeList
            }
        case actionTypes.ADD_NEW_EMPLOYEE:
            return {
                ...state,
                employees: action.payload.employeeList
            }
        case actionTypes.SEARCH: 
            return {
                ...state,
                searchValue: action.searchValue
            };
        case actionTypes.CHANGE_MODAL: 
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }


}

export default adminReducer;