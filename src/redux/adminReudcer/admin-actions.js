import * as actionTypes from './admin-types';
export const getEmployee = (employee) => {
    return {
        type: actionTypes.GET_EMPLOYEES,
        payload: {
            employeeList: employee
        }
    }
}
export const addNewEmployee = (employee) => {
    return {
        type: actionTypes.ADD_NEW_EMPLOYEE,
        payload: {
            id: employee.id,
            value: {employee}
        }
    }
}
export const removeEmployee = (employeeID) => {
    return {
        type: actionTypes.REMOVE_EMPLOYEE,
        payload: {
            id: employeeID
        }
    }
}
// export const adjustItemQty = (itemID, value) => {
//     return {
//         type: actionTypes.ADJUST_ITEM_QTY,
//         payload: {
//             id: itemID,
//             qty: value
//         }
//     }
// }

export const loadCurrentEmployee = (employeeID) => {
    return {
        type: actionTypes.LOAD_CURRENT_EMPLOYEE,
        payload: {
            id: employeeID
        }
    }
}

export const search = (value) => {
    return {
        type: actionTypes.SEARCH,
        searchValue: value
    };
}
export const changeModal = (payload) => {
    console.log('payload', payload);
    return {
      type: 'CHANGE_MODAL',
      payload,
    }
  }