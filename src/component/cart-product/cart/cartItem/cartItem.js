import React, { useState } from "react";
import './cartItem.scss';

import { connect } from "react-redux";
import {
  adjustItemQty,
  removeFromCart,
} from '../../../../redux/shopping/shopping-actions';

function CartItem({ item, adjustQty, removeFromCart }) {
    const [input, setInput] = useState(item.qty);

    const onChangeHandler = (e) => {
      
         var qtyInput = parseInt(e.target.value);
        setInput(qtyInput);
        adjustQty(item.id, qtyInput);
    };
    return (
        <div className="cart-item">
          <img
            className="cartItem-image"
            src={item.image}
            alt={item.title}
          />
          <div  className="cartItem-infor">
              <div className="cartItem-details">
                <p className="details-title">{item.productName}</p>
                <p className="details-price">$ {item.priceDeault}</p>
              </div>
              <div className="cartItem-actions">
                <div className="cartItem-qty">
                  <input
                    min="1"
                    type="number"
                    id="qty"
                    name="qty"
                    value={input}
                    onChange={onChangeHandler}
                    className="cartItem-qty-input"
                  />
                </div>
                <button onClick={() => removeFromCart(item.id)} className="actions-deleteItemBtn">X</button>
              </div>
          </div>
          
        </div>
      )

}
const mapDispatchToProps = (dispatch) => {
    return {
      adjustQty: (id, value) => dispatch(adjustItemQty(id, value)),
      removeFromCart: (id) => dispatch(removeFromCart(id)),
    };
  };
export default connect(null, mapDispatchToProps)(CartItem);