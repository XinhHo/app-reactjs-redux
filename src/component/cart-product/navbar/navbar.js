import React, { useState, useEffect } from 'react';
import { Link, withRouter } from 'react-router-dom';
import './navbar.scss';

import { connect } from "react-redux";
import { ShoppingCartOutlined } from '@ant-design/icons';

function Navbar({ cart }, props) {
  const [cartCount, setCartCount] = useState(0);
  const token = localStorage.getItem("token");
  console.log('token', token)

  useEffect(() => {
    let count = 0;
    cart.forEach((item) => {
      count += item.qty;
    });

    setCartCount(count);
  }, [cart, cartCount]);
  const redirectPage = () => {
    if (token === null) {
      console.log('token', token);
      return (
        <div className="">
          
          <Link to="/login">
            <h5 className="navbar__login">Login</h5>
          </Link>
        </div>
        
      )

    } else {
      console.log('token-1', token);
      return (
      <div>
          <Link to="/admin">
            <h5 className="navbar__logout">Admin</h5>
        </Link>
      </div> )

    }

  }

  return (
    <div className="navbar">
          {/* <Link to="/homePage">
            <h2 className="navbar__logo">Home Page</h2>
          </Link>
      {redirectPage()} */}
      <Link to="/homePage">
        <h2 className="navbar__logo">IPhone Shop</h2>
      </Link>
      {/* <Link to="/login">
        <h5 className="navbar__login">Login</h5>
      </Link>
      <Link to="/admin">
        <h5 className="navbar__logout">Admin</h5>
      </Link> */}
      <Link to="/cart">
        <div className="navbar__cart">
          <ShoppingCartOutlined className="cart-icon"/>
          <div className="cart__counter" >{cartCount}</div>
        </div>
      </Link>
    </div>
  );
} 
const mapStateToProps = (state) => {
  return {
    cart: state.shop.cart,
  };
};

export default withRouter(connect(mapStateToProps)(Navbar));