import './products.scss';
import React from "react";
import { getProducts } from '../../../../src/services/productService';
import { useEffect } from 'react';
import { loadProduct } from '../../../redux/shopping/shopping-actions';

import Product from './product/product';
import { connect } from 'react-redux';

function Products(props) {
    useEffect(() => {
        let mounted = true;
        getProducts()
            .then(products => {
                if (mounted) {
                    // setProducts(products)
                    props.fetchAllProducts(products);
                }
            })
        return () => mounted = false;
    }, [])
    return (
        <div className=" products">
        {
            props.products.map((product) => (
                    <Product key={product.id} product={product} />
            ))
        }
        </div>
    )
}
const mapSateToProps = (state) => {
    return {
        products: state.shop.products,
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
      fetchAllProducts: (products) => dispatch(loadProduct(products))
    };
  };
export default connect(mapSateToProps, mapDispatchToProps)(Products);