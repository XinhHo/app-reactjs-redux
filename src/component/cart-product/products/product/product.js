import './product.scss';
import React from "react";
import { Link } from "react-router-dom";
import { getProducts } from '../../../../services/productService';
import { useState, useEffect } from 'react';
import { connect } from "react-redux";
import {
  loadCurrentItem,
  addToCart
} from '../../../../redux/shopping/shopping-actions';

function Product({ product, addToCart, loadCurrentItem }) {
    const [products, setProducts] = useState([]);
    
    const getProduct = () => {
        return (
            <div className="cards">
                {
                    products.map(product => (
                        <div className="content-card">
                            <div className="images">
                                <img src={product.image} />
                            </div>
                            <div className="slideshow-buttons">
                                <div className="one"></div>
                                <div className="two"></div>
                                <div className="three"></div>
                                <div className="four"></div>
                            </div>
                            {/* <p className="pick">choose size</p> */}
                            {/* <div className="sizes">
                                <div className="size">5</div>
                                <div className="size">6</div>
                                <div className="size">7</div>
                                <div className="size">8</div>
                                <div className="size">9</div>
                                <div className="size">10</div>
                                <div className="size">11</div>
                                <div className="size">12</div>
                            </div> */}
                            <div className="product">
                                <h6>{product.productName}</h6>
                                <div>$ {product.priceDeault}</div>
                                {/* <p className="desc">The Nike Epic React Flyknit foam cushioning is responsive yet light-weight, durable yet soft. This creates a sensation that not only enhances the feeling of moving forward, but makes running feel fun, too.</p> */}
                                <div className="buttons">
                                    <button className="add" 
                                        onClick={() => addToCart(product.id)}>
                                        Add to Cart
                                    </button>
                                    <button className="like"><span>♥</span></button>
                                </div>
                            </div>
                     </div>
                    ))
                }
            </div>
        )
    }
    const productItem = () => {
        return (
            <div className="content-card">
                <div className="images">
                    <img src={product.image} />
                </div>
                <div className="slideshow-buttons">
                    <div className="one"></div>
                    <div className="two"></div>
                    <div className="three"></div>
                    <div className="four"></div>
                </div>
                {/* <p className="pick">choose size</p> */}
                {/* <div className="sizes">
                    <div className="size">5</div>
                    <div className="size">6</div>
                    <div className="size">7</div>
                    <div className="size">8</div>
                    <div className="size">9</div>
                    <div className="size">10</div>
                    <div className="size">11</div>
                    <div className="size">12</div>
                </div> */}
                <div className="product">
                    <h6>{product.productName}</h6>
                    <div>{product.priceDeault}</div>
                    {/* <p className="desc">The Nike Epic React Flyknit foam cushioning is responsive yet light-weight, durable yet soft. This creates a sensation that not only enhances the feeling of moving forward, but makes running feel fun, too.</p> */}
                    <div className="buttons">
                        <button className="add" 
                            onClick={() => addToCart(product.id)}>
                            Add to Cart
                        </button>
                        <button className="like"><span>♥</span></button>
                    </div>
                </div>
             </div>
        )
    }


    return(
        <div className="cards">
            {/* <div className="content">{getProduct()}</div> */}
            <div className="content">{productItem()}</div>
        </div>
        
    );
}
const mapDispatchToProps = (dispatch) => {
    return {
      addToCart: (id) => dispatch(addToCart(id)),
      loadCurrentItem: (item) => dispatch(loadCurrentItem(item)),
    };
  };
export default connect(null, mapDispatchToProps)(Product);