import './view-page.scss';
import logo from '../../asset/logo_1/primary.png';
function ViewPage() {
    return(
        <div className="view-page">
            <div className="view-page-header justify-content-center">
                <div className="view-page-header-navbar" >
                    <div className="view-page-header-navbar-logo">
                        <img className="view-page-header-navbar-logo-size" src={logo}></img>
                    </div>
                    <div className="view-page-header-navbar-menu">
                        <nav class="navbar navbar-expand-lg h-100">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarText">
                                <ul class="navbar-nav mr-auto">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#">Persyaratan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Cara Mengajukan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Fitur Produk</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Bunga dan Biaya</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>

                    </div>
                </div>
            </div>
            <div className="view-page-content container">
                <div className="view-page-content-slide">
                    <div className="view-page-content-slide-left">
                        <div className="view-page-content-slide-left-text">
                            <p className="view-page-content-slide-left-text-1">
                                Raih impianmu bersama
                            </p>
                            <p className="view-page-content-slide-left-text-2">
                                CommBank KTA
                            </p>
                            <p className="view-page-content-slide-left-text-3">
                                Pinjaman cepat, mudah dan terjangkau hingga 100juta!
                            </p>
                        </div>
                        <img width="572px" src={'https://www.commbank.co.id/Repository/PersonalLoanEmail/Slider/Walkin/Education.png'}></img>
                    
                    </div>
                    <div className="view-page-content-slide-right">
                        <div className="view-page-content-slide-right-Simulasikan-rencana">
                            Simulasikan rencana pinjamanmu*
                        </div>
                        <div className="view-page-content-slide-right-value">
                            <span>Rp<p className="text-color">30.000.000</p></span>
                        </div>
                        <div className="view-page-content-slide-right-scroll">
                            <div className="value-start">10jt</div>
                            <div className="scrollbar scrollbar-info">
                                <div className="force-overflow"></div>
                            </div>
                            <div className="value-end">100jt</div>

                        </div>
                    </div>
                </div>
                
            </div>
            <div className="view-page-footer"></div>
        </div>

    );
}
export default ViewPage;