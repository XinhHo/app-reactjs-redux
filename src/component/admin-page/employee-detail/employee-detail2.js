import './employee-detail.scss';
import React, { useState, useEffect, useCallback } from 'react';
import 'antd/dist/antd.css';
import { Avatar, Input, Select, DatePicker, Radio, Button,  Upload, Form, message } from 'antd';
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { useHistory } from "react-router-dom";
import { UserOutlined, LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { getEmployeeById, addNewEmployee, updateEmployee } from '../../../services/employeesService';
import moment from 'moment';

const SaveSchema = yup.object().shape({
    firstName: yup.string().required(),
    lastName: yup.string().required(),
    level: yup.string().required(),
    department: yup.string().required(),
    gender: yup.string(),
    avatar: yup.string()

  });

function EmployeeDetail(props) {
    let history = useHistory();
    const { register, handleSubmit, errors, setValue } = useForm({
        validationSchema: SaveSchema,
        mode: "onBlur",
        reValidateMode: "onBlur"
    });
    const [employeeDetail, setEmployeeDetail] =  useState({});
    const [gender, setGender] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [onboardStatus, setOnboardStatus] = useState('');
    const [birthDate, setBirthDate] = useState('');
    const [level, setLevel] = useState('');
    const [avatar, setAvatar] = useState('');
    const [department, setDepartment] = useState('');
    const [loading, setLoading] = useState(false);
    const [id, setId] = useState(false)
    const radioStyle = {
        display: 'block',
        height: '30px',
        lineHeight: '30px',
    };
    const dateFormat = 'DD/MM/YYYY';
    
    const { Option } = Select;
    
    useEffect(() => { 
        if (props && props.location.state) {
            const employeeId = props.location.state.employeeId;
            setId(employeeId);
            getEmployeeDetailById(employeeId);
        }
        register("firstName", {
          required: "firstName is a required",
        });
        register("lastName", {
            required: "lastName is a required",
          });
        register("level", {
            required: "level is a required",
        });
        register("department", {
            required: "department is a required",
        });
        register("gender", {});
        register("onboardStatus", {});
        register("avatar", {});
      }, [register]);
    const handleChangeAvatar = (info) => {
        console.log('info', info);
        if (info.file.status === 'uploading') {
            setLoading( true );
          return;
        }
        if (info.file.status === 'done') {
          // Get this url from response in real world.
          getBase64(info.file.originFileObj, imageUrl =>
            setAvatar(imageUrl),
            setValue("avatar", { shouldValidate: false }),
            setLoading(false)
          );
        }
      };
      const uploadButton = () => {
          return (
            <div>
                {loading ? <LoadingOutlined /> : <PlusOutlined />}
                <div style={{ marginTop: 8 }}>Upload</div>
            </div>
          )
      }
    const avatarUser = (avatar) => {

        return (
            <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                beforeUpload={beforeUpload}
                onChange={handleChangeAvatar}
                ref={register}
            >
            { avatar ? <Avatar src={avatar} /> : <div className="upload-a">{uploadButton()}</div> }
            </Upload>
           
            
        )

    }
    const onChangeFirstName = (e) => {
        setFirstName(e.target.value);
        setValue("firstName", e.target.value, { shouldValidate: true })
    };
    const onChangeLastName = (e) => {
        setLastName(e.target.value);
        setValue("lastName", e.target.value, { shouldValidate: true })
    };
    const onChangeDepartment = (e) => {
        setDepartment(e);
        setValue("department", e, { shouldValidate: true })
    };
    const onChangeBirthDate = (date, dateString) => {
        setBirthDate(dateString);
        setValue("birthDate", dateString, { shouldValidate: true })
    };
    const onChangeLevel = (e) => {
        setLevel(e);
        setValue("level", e, { shouldValidate: true })
    };
    const onChangeGender = (e) => {
        setGender(e.target.value);
        setValue("level", e.target.value, { shouldValidate: true })
    };
    
    const onSubmit = data => {
        console.log('data', data)
        console.log('employeeId', id)
        if (id) {
            updateDetailEmployee(id, data);
        } else {
            createEmployee(data)
        }
       
    };
    const getEmployeeDetailById = (id) => {
        getEmployeeById(id).then(employee => {
            setEmployeeDetail(employee);
            setFirstName(employee.firstName);
            setLastName(employee.lastName);
            setGender(employee.gender);
            setBirthDate(employee.birthDate);
            setLevel(employee.level);
            setAvatar(employee.avatar);
            setDepartment(employee.department);
        }).catch(err => {
            console.log(err);
        });
    }

    const createEmployee = (employee) => {
        console.log('employee', employee)
        const employeeData = {
            firstName: employee.firstName ? employee.firstName : '',
            lastName: employee.lastName ? employee.lastName : '',
            level: employee.level ? employee.level : '',
            department: employee.department ? employee.department : '',
            gender:employee.gender ? employee.gender : '',
            avatar:employee.avatar ? employee.avatar : '',
            onboardStatus: employee.onboardStatus ? employee.onboardStatus : '',
            birthDate: employee.birthDate ? employee.birthDate : '',
        }
        addNewEmployee(employeeData).then(employee => {
            history.push({ 
                pathname: '/employee',
                // pathname: '/employee-detail',
            });
        }).catch(err => {
            console.log(err);
        });
    }

    const updateDetailEmployee = (id, employee) => {
        const employeeData = {
            firstName: employee.firstName ? employee.firstName : employeeDetail.firstName,
            lastName: employee.lastName ? employee.lastName : employeeDetail.lastName,
            level: employee.level ? employee.level : employeeDetail.level,
            department: employee.department ? employee.department : employeeDetail.department,
            gender:employee.gender ? employee.gender : employeeDetail.gender,
            avatar:employee.avatar ? employee.avatar : employeeDetail.avatar,
            onboardStatus: employee.onboardStatus ? employee.onboardStatus : employeeDetail.onboardStatus,
            birthDate: employee.birthDate ? employee.birthDate : employeeDetail.birthDate,
        }
        }
        updateEmployee(id, employeeData).then(employee => {
            console.log('update data')
            history.push({ 
                pathname: '/employee',
            });
        }).catch(err => {
            console.log(err);
        });
    }


    return (
        <Form onSubmitCapture={handleSubmit(onSubmit)}>
            <div className="employee-infor">
                <div className="employee-detail">
                    <div className="avatar">
                        {avatarUser(avatar)}
                        {errors.avatar && <p>{errors.avatar.message}</p>}
                    </div>
                    <div className="details">
                        <div className="first-name">
                            <label>FirstName</label>
                            <div>
                                <Input 
                                    placeholder="Please input FirstName.... " 
                                    name="firstName" 
                                    value={firstName}
                                    onChange={(e) => onChangeFirstName(e)}
                                />
                                {errors.firstName && <p>{errors.firstName.message}</p>}
                            </div>
                        </div>
                        
                        <div className="last-name">
                            <label>LastName</label>
                            <div>
                                <Input 
                                    placeholder="Please input LastName.... " 
                                    name="lastName" 
                                    value={lastName} 
                                    ref={register} 
                                    onChange={(e) => onChangeLastName(e)}/>
                                {errors.lastName && <p>{errors.lastName.message}</p>}
                            </div>
                            
                        </div>
                        { birthDate && <div className="comp-birthDate">
                            <label>BirthDate</label>
                            <DatePicker 
                                defaultValue={birthDate && moment(birthDate, dateFormat)} 
                                onChange={ onChangeBirthDate } 
                                format={dateFormat} 
                                name="birthDate" 
                                ref={register}
                            />
                            {errors.birthDate && <p>{errors.birthDate.message}</p>}
                        </div>}
                        { !birthDate && <div className="comp-birthDate">
                            <label>BirthDate</label>
                            <DatePicker
                                onChange={ onChangeBirthDate } 
                                format={dateFormat} 
                                name="birthDate"
                                ref={register} 
                            />
                            {errors.birthDate && <p>{errors.birthDate.message}</p>}
                        </div>}
                        <div className="comp-gender">
                            <label>Gender</label>
                            <Radio.Group  
                                className="gender" 
                                value={gender}  
                                onChange={(e) => onChangeGender(e)}
                                name="gender"
                                ref={register}
                                >
                                <Radio style={radioStyle} value={" Male"}>Male</Radio>
                                <Radio style={radioStyle} value={"Female"}>Female</Radio>
                                <Radio style={radioStyle} value={"Other"}>Different</Radio>
                            </Radio.Group>
                            {errors.gender && <p>{errors.gender.message}</p>}

                        </div>
                        <div className="comp-status">
                            <label>Specialize</label>
                            <div>
                                <Select showSearch
                                    placeholder="Please to Select"
                                    value={department}
                                    ref={register}
                                    onChange={(e) => onChangeDepartment(e)}
                                    name="department"
                                > 
                                    <Option value="">None</Option>
                                    <Option value="SSC">SSC</Option>
                                    <Option value="HR">HR</Option>
                                    <Option value="IT Support">IT Support</Option>
                                    <Option value="Accounting">Accounting</Option>

                                </Select>
                                {errors.department && <p>{errors.department.message}</p>}
                            </div>
                        </div>
                            
                        
                        <div className="comp-level">
                            <label>Level</label>
                            <div>
                                <Select 
                                    placeholder="Please to Select"
                                    optionFilterProp="children"
                                    value={level}
                                    ref={register}
                                    onChange={(e) => onChangeLevel(e)}
                                    name="level"
                                > 
                                    <Option value="">None</Option>
                                    <Option value="level 1">Level 1</Option>
                                    <Option value="level 2">Level 2</Option>
                                    <Option value="level 3">Level 3</Option>
                                    <Option value="level 4">Level 4</Option>
                                    <Option value="level 5">Level 5</Option>

                                </Select>
                                {errors.level && <p>{errors.level.message}</p>}
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
                
                <div className="btn-group">
                        <Button type="primary" className="btn-save" htmlType='submit'>Save</Button>
                        <Button type="" className="btn-cancel">Cancel</Button>
                </div>
            </div> 
        </Form>
    )
    
}
function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}
function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
  }

export default EmployeeDetail;