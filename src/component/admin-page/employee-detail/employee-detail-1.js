import React , {useCallback, useMemo, useState} from "react";
import { useForm, Controller  } from "react-hook-form";
import * as yup from "yup";
import moment from 'moment';
import RadioGroup from '../../common/validate-yup/customs-field/radioGroup';
import ReactDatePicker from 'react-datepicker';

import './employee-detail.scss';

const useYupValidationResolver = (validationSchema) =>
  useCallback(
    async (data) => {
      try {
        const values = await validationSchema.validate(data, {
          abortEarly: false
        });

        return {
          values,
          errors: {}
        };
      } catch (errors) {
        return {
          values: {},
          errors: errors.inner.reduce(
            (allErrors, currentError) => ({
              ...allErrors,
              [currentError.path]: {
                type: currentError.type || "validation",
                message: currentError.message
              }
            }),
            {}
          )
        };
      }
    },
    [validationSchema]
  );

  function EmployeeDetail() {
    const [firstName, setFirstName] = useState('');
    const [employeeDetail, setEmployeeDetail] =  useState({});
    const [gender, setGender] = useState('');
    const [lastName, setLastName] = useState('');
    const [onboardStatus, setOnboardStatus] = useState('');
    const [birthDate, setBirthDate] = useState('');
    const [level, setLevel] = useState('');
    const [avatar, setAvatar] = useState('');
    const [department, setDepartment] = useState('');
    const [loading, setLoading] = useState(false);
    const [id, setId] = useState(false)
    const radioStyle = {
        display: 'block',
        height: '30px',
        lineHeight: '30px',
    };
    const dateFormat = 'DD/MM/YYYY';
    const genderOptions = [
        { label: "Male", value: " male" },
        { label: "Female", value: "female" },
        { label: "Different", value: "other" }
      ];

    const validationSchema = useMemo(
        () =>
          yup.object({
            firstName: yup.string().required("firstName is a required"),
            lastName: yup.string().required("lastName is a required"),
            level: yup.string().required("level is a required"),
            department: yup.string().required("department is a required"),
            gender: yup.string().required("gender is a required"),
            birthDate: yup.string().required('Date of Birth is required'),
            // .matches(/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$/, 'Date of Birth must be a valid date in the format '),
            onboardStatus: yup.string(),
            avatar: yup.string()
          }),
        []
      );
      const resolver = useYupValidationResolver(validationSchema);
      const { handleSubmit, errors, register, control } = useForm({ 
          resolver,
          defaultValues: {
            firstName: 'Loan',
            lastName: 'Nguyen',
            level: 'Level 3',
            department: 'SSC',
            gender:'Male',
            avatar:'',
            onboardStatus: 'work',
            birthDate: '12/12/2021'
          }
         });

   
    
    const onChangeFirstName = (e) => {
        setFirstName(e.target.value)
    };
    const onChangeLastName = (e) => {
        setLastName(e.target.value);
    };
    const onChangeDepartment = (e) => {
        setDepartment(e);
    };
    const onChangeBirthDate = (date, dateString) => {
        setBirthDate(dateString);
    };
    const onChangeLevel = (e) => {
        setLevel(e);
    };
    const onChangeGender = (e) => {
        setGender(e.target.value);
    };
    const onSubmit = data => {
        alert(JSON.stringify(data));
    };
    
  
    return (
        <form onSubmit={handleSubmit((data) => console.log(data))}>
            <div className="employee-infor">
                <div className="employee-detail">
                    <div className="avatar">
                        {/* {avatarUser(avatar)} */}
                        {/* {errors.avatar && <p>{errors.avatar.message}</p>} */}
                    </div>
                    <div className="details">
                        <div className="first-name">
                            <label>FirstName</label>
                            <div>
                                <input 
                                    name="firstName" 
                                    ref={register}
                                    className={`form-control ${errors.firstName ? 'is-invalid' : ''}`}
                                    onChange={e => onChangeFirstName(e)}
                                    />
                                {errors.firstName && <p>{errors.firstName.message}</p>}
                            </div>
                        </div>
                        <div className="last-name">
                            <label>LastName</label>
                            <div>
                                <input 
                                    name="lastName" 
                                    ref={register} 
                                    onChange={(e) => onChangeLastName(e)}
                                    className={`form-control ${errors.lastName ? 'is-invalid' : ''}`}
                                    />
                                {errors.lastName && <p>{errors.lastName.message}</p>}
                            </div>
                        </div>
                        { birthDate && <div className="comp-birthDate">
                            <div className="form-group col">
                                <label>Date of Birth</label>
                                <input 
                                    name="birthDate" 
                                    type="date"
                                    onChange={ onChangeBirthDate }
                                    ref={register} 
                                    format={dateFormat}
                                    defaultValue={birthDate && moment(birthDate, dateFormat)}
                                    className={`form-control ${errors.birthDate ? 'is-invalid' : ''}`} 
                                />
                                {errors.birthDate && <p>{errors.birthDate.message}</p>}
                            </div>
                           
                        </div>}
                        { !birthDate && <div className="comp-birthDate">
                        
                            <label>Date of Birth</label>
                            <div className="form-group col">
                                <input 
                                    name="birthDate" 
                                    type="date"
                                    onChange={ onChangeBirthDate }
                                    ref={register} 
                                    defaultValue={birthDate && moment(birthDate, dateFormat)}
                                    format={dateFormat}
                                    className={`form-control ${errors.birthDate ? 'is-invalid' : ''}`} 
                                />
                                {errors.birthDate && <p>{errors.birthDate.message}</p>}
                            </div>
                        </div>}
                        <div className="comp-gender">
                            <label>Gender</label>
                            <div>
                                <RadioGroup
                                    label="Gender"
                                    name="gender"
                                    options={ genderOptions }
                                    inputRef={register}
                                    className={`form-control ${errors.gender ? 'is-invalid' : ''}`}
                                    onChangeValue={(e) => onChangeGender(e)}
                                />
                                {errors.gender && <p>{errors.gender.message}</p>}
                            </div>
                            
                        </div>
                        <div className="comp-status">
                            <label>Specialize</label>
                            <div>
                                <select
                                    placeholder="Please to Select"
                                    ref={register}
                                    onChange={(e) => onChangeDepartment(e)}
                                    name="department"
                                    className={`form-control ${errors.department ? 'is-invalid' : ''}`}
                                > 
                                    <option value="">None</option>
                                    <option value="SSC">SSC</option>
                                    <option value="HR">HR</option>
                                    <option value="IT Support">IT Support</option>
                                    <option value="Accounting">Accounting</option>

                                </select>
                                {errors.department && <p>{errors.department.message}</p>}
                            </div>
                        </div>
                        <div className="comp-level">
                            <label>Level</label>
                            <div className="form-group col">
                                <select 
                                    placeholder="Please to Select"
                                    ref={register}
                                    onChange={(e) => onChangeLevel(e)}
                                    name="level"
                                    className={`form-control ${errors.level ? 'is-invalid' : ''}`}
                                > 
                                    <option value="">None</option>
                                    <option value="Level 1">Level 1</option>
                                    <option value="Level 2">Level 2</option>
                                    <option value="Level 3">Level 3</option>
                                    <option value="Level 4">Level 4</option>
                                    <option value="Level 5">Level 5</option>

                                </select>
                                {errors.level && <p>{errors.level.message}</p>}
                            </div>
                            
                        </div>
                    </div>

                </div>
                <div className="btn-group">
                        <button type="submit" className="btn btn-save btn-primary" >Save</button>
                        <button type="" className="btn btn-cancel">Cancel</button>
                </div>
            </div>
      </form>
    );
  }
export default EmployeeDetail;