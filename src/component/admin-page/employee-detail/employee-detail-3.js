import React from 'react';
import * as ReactDOM from 'react-dom';
import { Form, Input, InputNumber, Button } from 'antd';
import { withFormik, FormikErrors, FormikProps } from "formik";
import * as Yup from 'yup';

class RegForm extends React.Component {
  constructor(props) {
      super(props);
  }

  render(){
    const { values, handleChange, handleBlur, handleSubmit, touched, errors } = this.props;
    return(
     <Form onSubmit={handleSubmit}>
       <Form.Item
          help={touched.email && errors.email ? errors.email : ""}
          validateStatus={touched.email && errors.email ? "error" : "success"}
          label="E-mail"
          name="email"
       >
         <Input
            placeholder="E-mail"
            value={values.email}
            onChange={handleChange}
            onBlur={handleBlur}
          />
       </Form.Item>
       <Form.Item
          help={touched.age && errors.age ? errors.age : ""}
          validateStatus={touched.age && errors.age ? "error" : "success"}
          label="Age"
          name="age"
        >
          <InputNumber
             value={values.age}
             onChange={handleChange}
             onBlur={handleBlur}
          />
        </Form.Item>
        <Form.Item>
           <Button
              type="primary"
              htmlType="submit"
              className="form-submit-button"
            >
            Send
            </Button>
         </Form.Item>
     </Form>
    );
  }
}


const vSchema = Yup.object().shape({
  email: Yup.string()
        .email('Not valid mail')
        .required('Required'),
  age: Yup.number()
    .test({
      message:'From 18 to 65',
      test: value => {
        return (value >= 18 && value <=65)
      },
    })
    .required('Required')
});


const EmployeeDetail = withFormik({
    vSchema,
    mapPropsToValues: () => ({ email: '', age: ''}),
    handleSubmit: async (values, { props, setErrors }) => {
        const errors = await props.submit(values);
        if (errors) {
            setErrors(errors);
        }
    }
})(RegForm);

export default EmployeeDetail;