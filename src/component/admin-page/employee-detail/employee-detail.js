import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import EmployeeDetailView from './employee-detail-view/index';
import {
  dateFormat
} from '../../../common/validate-yup/fields-format/fields-format';
import moment from "moment";
import { getEmployeeById, addNewEmployee, updateEmployee } from '../../../services/employeesService';
import { useHistory } from "react-router-dom";
import capitalizeFirstLetter from '../../../common/share-service/capitalizeFirstLetter';

const initialDefaultValues = {
    firstName: "",
    lastName: "",
    birthDate: moment(Date.now()),
    onboardDate:  moment(Date.now()),
    gender:"Male",
    department: "",
    level: "",
    avatar: "",
    address:"",
    onboardStatus: 'Work',
    selectGenderOptions: ["Male", "Female", "Other"],
    selectDepartmentOptions: ["SSC", "HR", "IT Support", "Accounting"],
    selectLevelOptions: ["Level 1", "Level 2", "Level 3", "Level 4", "Level 5"]
};

 function EmployeeDetail(props) {
     let history = useHistory();
     const [initialValues, setInitialValues] = useState({});
     const [id, setId] = useState('');
    useEffect(() => {
        if (props.location.state) {
            const employeeId = props.location.state.employeeId;
            setId(employeeId);
            getEmployeeDetailById(employeeId);

        } else {
            setInitialValues(initialDefaultValues)
        }

    }, []);
    
    const getEmployeeDetailById = (id) => {
            getEmployeeById(id).then(employee => {
                const data = { ... initialDefaultValues,
                    firstName: employee.firstName || '',
                    lastName: employee.lastName || '',
                    birthDate:  employee.birthDate && moment(employee.birthDate, dateFormat) || moment(Date.now()).format(dateFormat),
                    gender: capitalizeFirstLetter(employee.gender) || "Male",
                    department: employee.department || '',
                    level: employee.level || '',
                    avatar: employee.avatar || '',
                    onboardStatus: employee.onboardStatus || '',
                    onboardDate: employee.onboardDate && moment(employee.onboardDate, dateFormat) || moment(Date.now()).format(dateFormat),
                    address: employee.address
                }
                setInitialValues(data);
                console.log('initialValues', initialValues);

        }).catch(err => {
            console.log(err);
        });
    }
    const createEmployee = (employee) => {
        addNewEmployee(employee).then(employee => {
            history.push({ 
                pathname: '/employee',
                // pathname: '/employee-detail',
            });
        }).catch(err => {
            console.log(err);
        });
    }
    const updateDetailEmployee = (id, employee) => {
        updateEmployee(id, employee).then(employee => {
            history.push({ 
                pathname: '/employee',
            });
        }).catch(err => {
            console.log(err);
        });
    }

    const handleSubmit = formProps => {
        const { firstName, lastName, birthDate, gender, department, level, onboardStatus, avatar, onboardDate, address } = formProps;
        const selectedDate = birthDate && moment(birthDate).format(dateFormat);
        const onboardDateSelect = onboardDate && moment(onboardDate).format(dateFormat) ||  moment(Date.now()).format(dateFormat);
        const data = {
            firstName,
            lastName,
            birthDate: `${selectedDate}`,
            gender: gender.target ? gender.target.value : gender ,
            department,
            level,
            onboardStatus,
            avatar: avatar ? avatar : '',
            onboardDate: `${onboardDateSelect}`,
            address,
        }
        if (id) {
            updateDetailEmployee(id, data);
        } else {
            createEmployee(data)
        }
  };

  console.log('init', initialValues)

  return (
    <Formik
        enableReinitialize
        initialValues={initialValues}
        onSubmit={e => handleSubmit(e)}
        render={EmployeeDetailView}
    />
  );
}
export default EmployeeDetail;