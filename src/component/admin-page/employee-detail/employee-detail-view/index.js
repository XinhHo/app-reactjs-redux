import React from "react";
import 'antd/dist/antd.css';
import '../employee-detail.scss';
import Avatar from '../../../../common/avatar/index';
import moment from "moment";

import { Form, Field } from "formik";
import {
  AntDatePicker,
  AntInput,
  AntSelect,
  AntRadio
} from '../../../../common/validate-yup/create-ant-fields/create-ant-fields';
import { dateFormat } from '../../../../common/validate-yup/fields-format/fields-format';
import {
  validateDate,
  isRequired
} from '../../../../common/validate-yup/validate-fields/validate-fields';
import { useHistory } from "react-router-dom";

export default ({ handleSubmit, values, submitCount, ...rest }) => {
    let history = useHistory();
    const onSetDataFromParent = (imageUrl) => {
        console.log("onSetDataFromParent")
        rest.setFieldValue('avatar',imageUrl)
    }
    const handleCancel = () => {
        history.push('/employee');
    }
    // console.log('values',rest);
    // console.log('values',values);
    return (
  <Form className="form-container" onSubmit={handleSubmit}>
      <div className="employee-infor">
            <div className="employee-detail">
                <div className="avatar">
                    <Avatar 
                        // onSetDataFromParent={onSetDataFromParent}
                        setFieldValue={rest.setFieldValue}
                        name="avatar"
                        value={values.avatar}
                    />
                </div>
                <div className="details">
                    <div className="first-name">
                    <Field
                        placeholder="Please input FirstName.... "
                        component={AntInput}
                        name="firstName"
                        type="text"
                        label="FirstName"
                        validate={isRequired}
                        submitCount={submitCount}
                        hasFeedback
                        />
                    </div>
                    <div className="last-name">
                        <Field
                            placeholder="Please input LastName.... "
                            component={AntInput}
                            name="lastName"
                            type="text"
                            label="LastName"
                            validate={isRequired}
                            submitCount={submitCount}
                            hasFeedback
                        />
                    </div>
                    <div className="comp-birthDate">
                            <Field
                                component={AntDatePicker}
                                name="birthDate"
                                label="Date Of Birth"
                                defaultValue={ values.birthDate && moment(values.birthDate, dateFormat)}
                                format={dateFormat}
                                validate={validateDate}
                                submitCount={submitCount}
                                hasFeedback
                            />
                    </div>
                    <div className="comp-gender">
                        <Field
                            component={AntRadio}
                            name="gender"
                            label="Gender"
                            defaultValue={values.gender}
                            selectRadio={values.selectGenderOptions}
                            validate={isRequired}
                            submitCount={submitCount}
                            tokenSeparators={[","]}
                            hasFeedback
                        />
                    </div>
                    <div className="comp-address">
                        <Field
                            placeholder="Please input address.... "
                            component={AntInput}
                            name="address"
                            type="text"
                            label="Address"
                            submitCount={submitCount}
                            hasFeedback
                        />
                    </div>
                    <div className="comp-status">
                        <Field
                            component={AntSelect}
                            name="department"
                            label="Specialize"
                            defaultValue={values.department}
                            selectOptions={values.selectDepartmentOptions}
                            validate={isRequired}
                            submitCount={submitCount}
                            tokenSeparators={[","]}
                            hasFeedback
                        />
                    </div>
                    <div className="comp-level">
                        <Field
                            component={AntSelect}
                            name="level"
                            label="Level"
                            defaultValue={values.level}
                            selectOptions={values.selectLevelOptions}
                            validate={isRequired}
                            submitCount={submitCount}
                            tokenSeparators={[","]}
                            hasFeedback
                        />
                    </div>
                </div>
            </div>
            <div className="btn-group">
                <button className="ant-btn ant-btn-primary" type="submit">Submit</button>
                <button type="" className="btn-cancel ant-btn" onClick={e => handleCancel()}>Cancel</button>
            </div>
      </div>
  </Form>

)};