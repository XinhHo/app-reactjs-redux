import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import { Line } from "react-chartjs-2";
import { chartColors } from '../../../../common/colors';
import './lineChart.scss';

function LineChart() {
    const options = {
        title:{
            display:true,
            text:'Employees By Level',
            fontSize:20,
        },
        legend: {
          display: true,
          position: "bottom"
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        }
      };

      const data = {
        maintainAspectRatio: false,
        responsive: false,
        labels: ["Level 1", "Level 2", "Level 3", "Level 4", "Level 5", "Level 6"],
        datasets: [
          {
            label: 'Level',
            data: [4000, 3000,2500, 2000, 1000, 500],
            backgroundColor: chartColors,
            hoverBackgroundColor: chartColors
          }
        ]
      };
      
      return (
        <div className="line-chart">
            <Line
                data={data}
                options={options}
            />
        </div>
        
      
      )

}

export default LineChart;

