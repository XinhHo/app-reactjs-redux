import './barChart.scss';
import { Bar } from 'react-chartjs-2';
import React, { useState, useEffect } from 'react';

function BarChart() {
    const dataChart = {
        labels: ['Jan', 'Feb', 'Mar','Apr', 'May', 'June', 'July', 'Oct', 'Sep', 'Nov', 'Dec'],
        datasets: [{
            label: 'Empployee',
            backgroundColor: 'rgba(75,192,192,1)',
            borderColor: 'rgba(0,0,0,1)',
            borderWidth: 2,
            data: [65, 59, 80, 81, 56, 0, 10, 5, 10, 25, 20, 35]
        }]
    }
    
    const chartConfig = () => {
        return (
            <Bar
                data={dataChart}
                options={{
                    title:{
                        display:true,
                        text:'New Empployee per month',
                        fontSize:20
                    },
                    legend:{
                        display:true,
                        position:'bottom'
                    }
                }}
                />
        )

    }
    return (
        <div className="bar-chart">
            {chartConfig()}
        </div>
    )

}
export default BarChart;