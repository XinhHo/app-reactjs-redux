import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import { Pie } from "react-chartjs-2";
import { chartColors } from '../../../../common/colors';
import './pieChart.scss';

function PieChart() {
    const options = {
        title:{
            display:true,
            text:'Employees By Department',
            fontSize:20,
        },
        legend: {
          display: true,
          position: "bottom"
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        }
      };

      const data = {
        maintainAspectRatio: false,
        responsive: false,
        labels: ["accounting", "HR", "SSC", "IT Support"],
        datasets: [
          {
            data: [300, 50, 100, 50],
            backgroundColor: chartColors,
            hoverBackgroundColor: chartColors
          }
        ]
      };
      
      return (
        <div className="pie-chart">
            <Pie
                data={data}
                options={options}
            />
        </div>
        
      
      )

}

export default PieChart;

