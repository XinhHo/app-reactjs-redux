import './admin.scss';
import { connect } from "react-redux";
import BarChart from './barChart/barChart';
import PieChart from './pieChart/pieChart';
import LineChart from './lineChart/lineChart';
import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router';


function Admin() {
    
    return (
        <div className="dash-board">
            <div className="dash-board-card-left">
                <BarChart />
                <LineChart />
            </div>
            <div className="dash-board-card-left">
                <PieChart />
            </div>
        </div>
       
    )
    
}
const mapStateToProps = (state) => {
    return {
        state
    };
  };
export default connect(mapStateToProps)(Admin);