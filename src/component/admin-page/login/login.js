import './login.scss';
import 'antd/dist/antd.css';
import * as React from "react";
import {  } from "module";
import { useEffect, useState} from 'react';
import { connect } from "react-redux";
import { useHistory } from 'react-router-dom';
import { Form, Input, Button, Checkbox } from 'antd';
import { withFormik, FormikErrors, FormikProps } from "formik";
import * as yup from "yup";

const FormItem = Form.Item;
const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };
  const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
  };

function Login(props) {
    let history = useHistory();
    const [username, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [loggedIn, setLoggedIn] = useState(false);

    const adminUser = {
        email: "admin@admin.com",
        password: "admin123"
      }
    const onFinish = (values) => {
        console.log('Success:', values);
        if (username == adminUser.email && password == adminUser.password) {
            setLoggedIn(true);
            localStorage.setItem("token", "fgdfgdherrtyjkjklkjhk");
            // localStorage.clear
            history.push('/admin')
         }
      };
    
      const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
      };
    

    return (
        <Form
      {...layout}
      name="basic"
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <FormItem
        label="Username"
        name="username"
        rules={[{ required: true, message: 'Please input your username!' }]}
        values={username} 
        onChange = {(e) => setUserName(e.target.value)}
      >
        <Input />
      </FormItem>

      <FormItem
        label="Password"
        name="password"
        values={password} 
        onChange = {(e) => setPassword(e.target.value)}
        rules={[{ required: true, message: 'Please input your password!' }]}
      >
        <Input.Password />
      </FormItem>

      <FormItem {...tailLayout} name="remember" valuePropName="checked">
        <Checkbox>Remember me</Checkbox>
      </FormItem>

      <FormItem {...tailLayout}>
        <Button type="primary" htmlType="submit" >
          Submit
        </Button>
      </FormItem>
    </Form>
    )
    
}
const mapStateToProps = (state) => {
    return {
        state
    };
  };
export default connect(mapStateToProps)(Login);