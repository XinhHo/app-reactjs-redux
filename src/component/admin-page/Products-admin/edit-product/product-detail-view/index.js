import './product-detail-view.scss';
import React, { useState }  from "react";
import 'antd/dist/antd.css';
import Avatar from '../../../../../common/avatar/index';
import moment from "moment";

import { Form, Field } from "formik";
import {
    AntDatePicker,
    AntInput,
    AntTextArea
} from '../../../../../common/validate-yup/create-ant-fields/create-ant-fields';
import { dateFormat } from '../../../../../common/validate-yup/fields-format/fields-format';
import { useHistory } from "react-router-dom";
import ViewAddOption from '../../view-option/index';

export default ({ handleSubmit, values, submitCount, errors, touched, ...rest }) => {
    let history = useHistory();
    
    const onSetDataFromParent = (imageUrl) => {
        console.log("onSetDataFromParent")
        rest.setFieldValue('image',imageUrl)
    }
    const handleCancel = () => {
        history.push('/products');
    }
    //  console.log('values',rest);
    // console.log('values',values);
    
    return (
    <Form className="form-container" onSubmit={handleSubmit}>
      <div className="product-infor">
            <div className="product-detail">
                <div className="avatar">
                    <Avatar shape="square"
                        // onSetDataFromParent={onSetDataFromParent}
                        setFieldValue={rest.setFieldValue}
                        name="image"
                        value={values.image}
                    />
                </div>
                <div className="details">
                    <div className="details-infor">
                        <div className="product-name">
                            <Field
                                placeholder="Please input Product Name.... "
                                component={AntInput}
                                name="productName"
                                type="text"
                                label="Product Name"
                                validate={errors.productName || touched.productName}
                                submitCount={submitCount}
                                hasFeedback
                            />
                        </div>
                        <div className="price">
                            <Field
                                placeholder="Please input price.... "
                                component={AntInput}
                                name="priceDeault"
                                type="text"
                                label="Price Deault"
                                validate={errors.priceDeault || touched.priceDeault}
                                submitCount={submitCount}
                                hasFeedback
                            />
                        </div>
                        <div className="expired-date">
                            <Field
                                component={AntDatePicker}
                                name="expiredDate"
                                label="Expired Date"
                                defaultValue={ values.expiredDate && moment(values.expiredDate, dateFormat)}
                                format={dateFormat}
                                validate={errors.expiredDate || touched.expiredDate}
                                submitCount={submitCount}
                                hasFeedback
                            />
                        </div>
                        <div className="amount">
                            <Field
                                placeholder="Please input amount.... "
                                component={AntInput}
                                name="amount"
                                type="text"
                                label="Amount"
                                submitCount={submitCount}
                                hasFeedback
                            />
                        </div>
                        <div className="description">
                            <Field
                                placeholder="Please input description.... "
                                component={AntTextArea}
                                name="description"
                                type="textArea"
                                label="Description"
                                submitCount={submitCount}
                                hasFeedback
                            />
                        </div>
                    </div>
                    
                    <div className="add-opptions">
                        <label>Choose options</label>
                        <ViewAddOption 
                            setFieldValue={rest.setFieldValue}
                            name="optionInformation"
                            value={values.optionInformation}
                        />
                    </div>
                </div>
            </div>
            <div className="btn-group">
                <button className="ant-btn ant-btn-primary" type="submit">Submit</button>
                <button type="" className="btn-cancel ant-btn" onClick={e => handleCancel()}>Cancel</button>
            </div>
      </div>
    </Form>

)};