import './edit-product.scss';
import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import ProdectDetailView from './product-detail-view/index';
import {
  dateFormat
} from '../../../../common/validate-yup/fields-format/fields-format';
import moment from "moment";
import * as Yup from 'yup';
import { useHistory, useLocation } from "react-router-dom";
import {getProductById, addNewProduct, updateProduct } from '../../../../services/productService'

const initialValuesDefault = {
    productName: "",
    priceDeault: "",
    expiredDate: moment(Date.now()),
    amount:"",
    description: "",
    image: "",
    optionInformation: {}
};
const DetailProductSchema = Yup.object().shape({
    productName: Yup.string()
      .min(10, 'Too Short!')
      .max(60, 'Too Long!')
      .required('Required'),
    priceDeault: Yup.string()
        .matches(/^[0-9][A-Za-z0-9 -]*$/, 'You should fill from 0 to 9')
        .required('Required'),
    expiredDate: Yup.date().required('Required'),
    amount: Yup.string()
        .matches(/^[0-9][A-Za-z0-9 -]*$/, 'You should fill from 0 to 9')
        .required('Required'),
    description: Yup.string().required('Required')
  });

function EditProduct(props) {
    let history = useHistory();
    const [initialValues, setInitialValues] = useState({});
     const [id, setId] = useState('');
     useEffect(() => {
        if (props.location.state) {
            const productId = props.location.state.productId;
            setId(productId);
            getProductDetailById(productId);

        } else {
            setInitialValues(initialValuesDefault)
        }

    }, []);

    const getProductDetailById = (id) => {
        getProductById(id).then(product => {
            const data = { ... initialValuesDefault,
                productName: product.productName || '',
                priceDeault: product.priceDeault || '',
                expiredDate: product.expiredDate && moment(product.expiredDate, dateFormat) || moment(Date.now(), dateFormat),
                amount: product.amount || '',
                description: product.description || '',
                image: product.image || '',
                optionInformation: product.optionInformation || {} 
            }
            setInitialValues(data);
            console.log('initialValues', initialValues);

        }).catch(err => {
            console.log(err);
        });
    }
    const createProduct = (product) => {
        addNewProduct(product).then(product => {
            history.push({ 
                pathname: '/products',
                // pathname: '/employee-detail',
            });
        }).catch(err => {
            console.log(err);
        });
    }
    const updateDetailProduct = (id, product) => {
        updateProduct(id, product).then(product => {
            history.push({ 
                pathname: '/products',
            });
        }).catch(err => {
            console.log(err);
        });
    }

    const handleSubmit = formProps => {
        const { productName, priceDeault, expiredDate, amount, description, image, optionInformation } = formProps;
        const selectedDate = expiredDate && moment(expiredDate).format(dateFormat) || moment(Date.now());
        const data = {
            productName,
            priceDeault,
            expiredDate: selectedDate,
            amount,
            description,
            image,
            optionInformation
        }
        console.log('data-product', data)
        if (id) {
            updateDetailProduct(id, data);
        } else {
            createProduct(data)
        }
  };
    return (
        <Formik
        enableReinitialize
        initialValues={initialValues}
        validationSchema={DetailProductSchema}
        onSubmit={e => handleSubmit(e)}
        render={ProdectDetailView}
    />
    )
}
export default EditProduct;