import './products-admin.scss';
import React, { useState, useEffect } from 'react';
import 'antd/dist/antd.css';
import { Table, Popconfirm, Space, Spin, Button, Avatar } from 'antd';
import { getProducts } from '../../../services/productService';
import { connect } from 'react-redux';
import { AudioOutlined, DeleteOutlined, EditOutlined, UserOutlined } from '@ant-design/icons';
import { loadProduct } from '../../../redux/shopping/shopping-actions';
import { useHistory } from "react-router-dom";
import { withRouter } from 'react-router';


function ProductAdmin(props) {
    let history = useHistory();
    const [products, setProducts] = useState([]);
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    useEffect(() => {
        getListData();
    }, [])
    const getListData = () => {
        let mounted = true;
        getProducts()
            .then(product => {
                if (mounted) {
                    props.fetchAllProducts(product);
                    setProducts(product);
                }
            })
        return () => mounted = false;
    }
    const edit = (record) => {
        console.log('record', record);
        history.push({ 
            pathname: `/product-detail/${record.id}`,
            state:{
                productId: record.id
            }
        });
    }
    const addNewProduct = () => {
        history.push("/product-detail");
    };
    const handleDelete = (key) => {
        const dataSource = [...products];
        setProducts({
            products: dataSource.filter((item) => item.key !== key),
        });
      };
    const avatarUser = (avatar) => {
        if(avatar && avatar !== ''){
            return  (<Avatar shape="square" src={avatar}/>)
        } else{
            return (<Avatar shape="square" icon={<UserOutlined />}/>)
        }

    }
    const rowSelection = {
        selectedRowKeys,
        onChange: () => onSelectChange(),
    };
    const onSelectChange = (selectedRowKeys) => {
        setSelectedRowKeys({ selectedRowKeys });
    };
    const columns = [
        {
            title: 'Avatar',
            dataIndex: 'image',
            key: 'image',
            editable: false,
            render: (image) => avatarUser(image)
          },
        {
          title: 'ProductName',
          dataIndex: 'productName',
          key: 'productName',
          editable: false,
        },
        {
          title: 'Price',
          dataIndex: 'priceDeault',
          key: 'priceDeault',
          editable: false,
        },
        {
          title: 'Expired Date',
          dataIndex: 'expiredDate',
          key: 'expiredDate',
          editable: false,
        },
        {
            title: 'Amount',
            dataIndex: 'amount',
            key: 'amount',
            editable: false,
          },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
            editable: false,
        },
        {
            title: 'Actions',
            dataIndex: 'actions',
            key: 'actions',
            align: 'center',
            render: (_, record) =>
                <Space size="middle">
                    <>
                    
                        { products.length >= 1 ? (
                            <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record.key)}>
                                <div className="btn-delete"><DeleteOutlined /></div>
                            </Popconfirm>
                            ) : null 
                        }
                        {
                            <EditOutlined onClick={() => edit(record)}/>
                        }
                    
                    </>
                </Space>
                
        },
      ];
    return (
        <div className="product-list">
            <div className="btn-new">
                <Button type="primary" onClick={() => addNewProduct()}>New Product</Button>
            </div>
            <div>
                <Spin tip="Loading..." spinning={isLoading}>
                    <Table className="table"
                        rowSelection={rowSelection}
                        columns={columns}
                        dataSource={products}
                        onRow={(record, rowIndex) => {
                            return {
                                onDoubleClick: () => edit(record), // click row
                            };
                        }}
                    />
                </Spin>
            </div>
        </div>
    )
}
const mapSateToProps = (state) => {
    return {
        products: state.shop.products,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
      fetchAllProducts: (products) => dispatch(loadProduct(products))
    };
  };
export default connect(mapSateToProps, mapDispatchToProps)(ProductAdmin);