import React, { useContext, useState, useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './view-option.scss';
import { Table, Space } from 'antd';
function MakeOptionTable(props) {
  console.log('props', props);
  const titleColumn = props.matchOption.map(item => item.nameOption);
  const data1 = titleColumn.map(item => {
    return {
      title: item.toUpperCase(),
      dataIndex: item,
      key: item.toLowerCase(),
      // render: text => <a>{text}</a>,
    }
  });
  console.log('data1', data1)
  const columns = [
    ...data1,
    {
      title: 'Price',
      dataIndex: 'priceOption',
      key: 'price',
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <Space size="middle">
          <a>Edit</a>
          <a>Delete</a>
        </Space>
      ),
    },
  ];
  
  const data = props.optionList;
  return (
    <Table columns={columns} dataSource={data} />
  )
}
export default MakeOptionTable;
