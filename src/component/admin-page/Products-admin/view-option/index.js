import React, { useState } from "react";
import './view-option.scss';
import "antd/dist/antd.css";
import { Input, Button } from 'antd';
import OptionList from '../add-option/index';
import OptionChild from './radio-list';
import { MinusCircleFilled, PlusSquareFilled, CheckSquareFilled, CheckCircleOutlined } from '@ant-design/icons';
import "antd/dist/antd.css";
import MakeOptionTable from './make-option-table';
function ViewAddOption(props) {
    const [optionDetails, setOptionsParents] = useState([]);
    const [matchOption, setMatchOption] = useState([]);
    const [optionsData, setOptionChoosed] = useState([]);
    const [makeOption, setMakeOption] = useState([]);
    const [priceOption, setPriceOption] = useState('');
    const handleChange = (e) => {
        if (["optionName"].includes(e.target.name)) {
            let optionDetailsOnChange = [...optionDetails];
            optionDetailsOnChange[e.target.dataset.id][e.target.name] = e.target.value;
            setOptionsParents(optionDetailsOnChange);
        }
    }
    const handleChangeChirld = (e, indexParent) => {
        let optionDetailsOnChange = [...optionDetails];
        if (indexParent) {
            const data = optionDetailsOnChange.map((item) => {
                if (item.index === indexParent) {
                    const dataOptions = [...item.optionList];
                    dataOptions[e.target.dataset.id][e.target.name] = e.target.value;
                    return {
                        ...item,
                        optionList: dataOptions
                    }
                }
                return item;
            });
            setOptionsParents(data);
        }
    };
    const addNewRow = (indexParent) => {
        let data = [...optionDetails];
        if (indexParent) {
            const dataAfter = data.map((item) => {
                if (item.index === indexParent) {
                    return {
                        ...item,
                        optionList: [...item.optionList,{ index: Math.random(), nameChirld: '', onFocusInput: false, isChoose: false}]
                    }
                }
                return item;
            });
            setOptionsParents(dataAfter);
        }
         else {
            const data1 = [...data, { index: Math.random(), optionName: '', optionList: [], onFocusInput: false}];
            setOptionsParents(data1);
        }
    };
    const handleOK = (val, indexParent) => {
        const data = [...optionDetails];
        if (indexParent) {
            const itemsParent = data.map((item) => {
                if (item.index === indexParent) {
                    const listChirld = item.optionList.map((itemChirld) => {
                        if(itemChirld.index === val.index) {
                            return {...itemChirld, onFocusInput: true}
                        }
                        return itemChirld;
                    })
                    return {...item, optionList: listChirld}
                }
                return item;
            });
            setOptionsParents(itemsParent);
        } else {
            const dataOff = data.map((item) => {
                if (item.index === val.index) {
                  return {
                    ...item,
                    onFocusInput: true
                  }
                }
                return item;
              });
            setOptionsParents(dataOff);
        }
        makeOptionData();
    }
    const deteteRow = (row, indexParent) => {
        const tempArr = [...optionDetails];
        if (indexParent) {
            const itemsParent = tempArr.map((item) => {
                if (item.index === indexParent) {
                    const listChirld = item.optionList.filter(optionChild => optionChild.index !== row.index)
                    return {...item, optionList: listChirld}
                }
                return item;
            });
            setOptionsParents(itemsParent);
        } else {
            const tempArrFilter = tempArr.filter(item => item.index !== row.index);
            setOptionsParents(tempArrFilter);
        }
    };
    const handleSubmit = () => {
        // console.log('hello')
    }
    const handleChildren = (indexParent) => {
        const dataList = [...optionDetails];
        const itemParent = dataList.filter(item => item.index === indexParent);
        const optionsByParent = itemParent[0].optionList;
        return (
            <div >
                <div className="add-form-chirld">
                    { optionsByParent.length !== 0 &&
                         <div className="container-option-as">
                            {renderNewCom(indexParent, optionsByParent)}
                        </div>
                    }   
                    <PlusSquareFilled style={{ color: '#0d6efd' }} onClick={() => addNewRow(indexParent)}/>
                </div>
            </div>
        )
    }

    const onChooseOption = (index, indexParent) => {
        const data = [...optionDetails];
        const itemsParent = data.map((item) => {
            if (item.index === indexParent) {
                const listChirld = item.optionList.map((itemChirld) => {
                    if(itemChirld.index === index) {
                        return {...itemChirld, isChoose: true }
                    }
                    return {...itemChirld, isChoose: false};
                })
                return {...item, optionList: listChirld}
            }
            return item;
        });
        const data1 = [...itemsParent];
        const matchOptions = data1.map((item) => {
            const itemChoosed = item.optionList && item.optionList.filter(itemChild => itemChild.isChoose).map(item => item.nameChirld);
            return {
                nameOption: item.optionName,
                optionChoosed: itemChoosed[0]
            }

        })
        // console.log('matchOptions',matchOptions);
        setOptionsParents(itemsParent);
        setMatchOption(matchOptions);

    }
    const makeOptionData = () => {
        const data1 = [...optionDetails];
        const mapData = data1.map((item) => {
            return {
                optionName: item.optionName,
                options: item.optionList && item.optionList.map(itemChild => itemChild.nameChirld)
            }
        })
        // console.log('mapData', mapData)
        setMakeOption(mapData);
    }
    const saveOption = () => {
        const data = [...matchOption];
        const options = {};
        data.map((item) => {
            options[item.nameOption] = item.optionChoosed;
        })
        // const data1 = data.map(item => item.optionChoosed);
        // console.log('options', options)
        
        const data1 = [...optionsData, {...options, priceOption}];
        // console.log('data1', data1)
        setOptionChoosed(data1)
        // debugger
        const optionInformation = {
            options: data1,
            makeOption: makeOption
        }
        console.log('optionInformation', optionInformation);
        props.setFieldValue(props.name, optionInformation);
    }
    // console.log('optionsData', optionsData)
    
    const ChangePriceByOption = (e) => {
        setPriceOption(e.target.value);
    }
    const addNewOption = () => {}

    const renderNewCom = (indexParent, optionsByParent) => {
        return optionsByParent.map((val, index) => {
            let name = `chirld-${index}`;
            return (
                <div className="add-form-row-child" key={val.index} >
                        <div className={!val.onFocusInput ? 'hideOption' : ''}>
                            <div className={(val.isChoose ? 'active' : '') + " option-item"} >
                                <div className="item" onClick={e => onChooseOption(val.index, indexParent)}>
                                    <CheckCircleOutlined  className="icon-ant"/>
                                </div>
                                <div className="item">{val.nameChirld}</div>
                                <MinusCircleFilled 
                                    className="item"
                                    style={{ color: '#dc3545' }} 
                                    onClick={() => deteteRow(val, indexParent)}
                                />
                            </div>
                        </div>
                        {
                            !val.onFocusInput && <form className="form-row-item-child" onChange={e => handleChangeChirld(e, indexParent)}>
                                <Input
                                className="option-name-chirld"
                                placeholder="Name"
                                name="nameChirld"
                                data-id={index}
                                id={name}
                            />
                            <CheckSquareFilled style={{ color: '#52c41a' }} onClick={() => handleOK(val, indexParent)}/>
                            </form>
                        }
                </div>

            );
        })
    }
    return (
        <div className="content">
            <div className="content-right">
                <form className="add-form" onSubmit={e => handleSubmit()} onChange={e => handleChange(e)}>
                    <div className="add-form-item">
                        <div className="container-option">
                            <OptionList
                                delete={deteteRow}
                                optionDetails={optionDetails}
                                handleChildren={handleChildren}
                                handleChange={handleChange}
                                handleOK={handleOK}
                            />
                        </div>
                    </div>
                    <div className="button-group">
                        <Button onClick={() => addNewRow()} type="primary" className="button-add">
                            Add
                        </Button>
                        {/* <Button onClick={() => addNewOption()}type="button" className="button btn-setPrice">Set Price</Button> */}
                    </div>
                </form>
            </div>
            <div className="content-center"></div>
            <div className="content-left">
                <div>Option Price:</div>
                <div className="content-left-items">
                        <div className="item">
                            <div className="item-detail">
                                {
                                    matchOption && matchOption.map((item) => {
                                        return (<div className="item-detail item-" >{item.optionChoosed}</div>) 
                                    })
                                }
                                <Input 
                                    className="item-detail item-price"
                                    placeholder="Price"
                                    name="priceByOption"
                                    onChange={e => ChangePriceByOption(e)}
                                />
                                <div className="group-btn">
                                    <Button onClick={() => saveOption()} type="primary" className="button">OK</Button>
                                </div>
                            </div>
                        </div>
                    <MakeOptionTable matchOption={matchOption} optionList={optionsData}/>
                </div>
            </div>
        </div>
    )

}
export default ViewAddOption;