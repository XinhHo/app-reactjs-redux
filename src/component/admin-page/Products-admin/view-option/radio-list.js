import React, { useState } from "react";
import './view-option.scss';
import { MinusCircleFilled, CheckCircleOutlined } from '@ant-design/icons';
import "antd/dist/antd.css";

function OptionChild(props) {
  console.log('props', props);
  const [indexChoose, setIndexChoose] = useState();
  const onChooseOption = (index) => {
    setIndexChoose(index);
  }
  return (
    <div className={!props.data.onFocusInput ? 'hideOption' : ''}>
      <div className={props.data.index === indexChoose ? 'active' : ''} onClick={e => onChooseOption(props.data.index)}>
          <CheckCircleOutlined className="icon-ant"/>
          {props.data.nameChirld}
          <MinusCircleFilled 
              style={{ color: '#dc3545' }} 
              onClick={() => props.deteteRow(props.data, props.optionsByParent)}
          />
      </div>
    </div>
  )
  
}
export default OptionChild;