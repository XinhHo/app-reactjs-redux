
import React, { useEffect, useState }  from "react";
import "antd/dist/antd.css";
import {MinusCircleFilled, CheckSquareFilled, RightCircleOutlined} from '@ant-design/icons';
import { Input } from 'antd';
function OptionList(props) {
    // console.log('props', props)
  return props.optionDetails.map((val, idx) => {
    let name = `name-${idx}`;
    // console.log('val', val)
    return (
      <div key={idx} data-id={idx}>
        {!val.onFocusInput && <div className="add-form-row">
              <div className="form-row" key={val.index}>
                <div className="">
                  <Input
                      className="option-name-parent"
                      placeholder="Name"
                      name="optionName"
                      data-id={idx}
                      id={name}
                  />
                </div>
                <CheckSquareFilled style={{ color: '#52c41a' }} onClick={() => props.handleOK(val)}/>
             </div>
            </div> 
        }
        {
          val.onFocusInput && <div className="add-form-row">
              <div className="form-row" key={val.index} >
                <RightCircleOutlined /><div name="name" className="optionName-parent" data-id={idx} id={name}> {val.optionName}</div>
                <MinusCircleFilled style={{ color: '#dc3545' }} onClick={() => props.delete(val )}/>
              </div>
              <div className="form-row-child">
                {props.handleChildren(val.index)}
              </div>
            </div>
        }
      </div>
    
    );
  });
};
export default OptionList;