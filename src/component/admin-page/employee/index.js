import './employee.scss';
import React, { useState, useEffect } from 'react';
import 'antd/dist/antd.css';
import { Table, Modal, Popconfirm, Space, Avatar, Spin, Button } from 'antd';
import { connect } from 'react-redux';
import { useHistory } from "react-router-dom";
import { getEmployee, changeModal } from '../../../redux/adminReudcer/admin-actions';
import { getListEmployee, updateEmployee } from '../../../services/employeesService';
import { AudioOutlined, DeleteOutlined, EditOutlined, UserOutlined } from '@ant-design/icons';
import SearchComponent from '../../../common/search/search';
import ModalDefault from '../../../common/update-modal/update-modal';
import { withRouter } from 'react-router';

function Employee(props) {
    let history = useHistory();
    const [employees, setEmployees] = useState([]);
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [employeeArr, setEmployeeArr] = useState([]);
  
    useEffect(() => {
        getListData();
    }, [])
    const handleDelete = (key) => {
        const dataSource = [...employees];
        setEmployees({
            employees: dataSource.filter((item) => item.key !== key),
        });
      };
    const edit = (record) => {
        console.log('record', record);
        history.push({ 
            pathname: `/employee-detail/${record.id}`,
            // pathname: '/employee-detail',
            state:{
                employeeId: record.id
            }
        });
    }
    const avatarUser = (avatar) => {
        if(avatar && avatar !== ''){
            return  (<Avatar src={avatar}/>)
        } else{
            return (<Avatar icon={<UserOutlined />}/>)
        }

    }

   
    const getListData = () => {
        let mounted = true;
        getListEmployee()
            .then(employeeList => {
                if (mounted) {
                    props.fetchAllEmployees(employeeList);
                    const arr = employeeList.map((emp) => {
                        return {
                            ...emp,
                            name: `${emp.firstName} ${emp.lastName}`
                        }
                    })
                    console.log('arr', arr)
                    setEmployeeArr(arr);
                    setEmployees(arr);
                    
                }
            })
        return () => mounted = false;
    }
    const updateDetailEmployee = (id, employee) => {
        console.log('employee-update', employee)
        updateEmployee(id, employee).then(employee => {
            getListData();
        }).catch(err => {
            console.log(err);
        });
    }
    const handleOk = (record) => {
        console.log('record', record)
        if (record.onboardStatus.toLowerCase() === 'work') {
            updateDetailEmployee(record.id, {...record, onboardStatus: 'Leave'})
           
        } else {
            updateDetailEmployee(record.id, {...record, onboardStatus: 'Work'})
        }
      };
    const handleCancelModal = (isHide) => {
        const currentModal = {
            visible: isHide
        }
        props.handleChangeModal({ modal: currentModal });
    }; 
    const changeModal = (record) => {
        const currentModal = {
          visible: true,
          title: 'Change Onboard Status',
          modalText: `You would like to change onboard status of ${record.firstName} ${record.lastName}`,
          rowData: record
        }
        props.handleChangeModal({ modal: currentModal });
    }
    
    const columns = [
        {
            title: 'Avatar',
            dataIndex: 'avatar',
            key: 'avatar',
            editable: false,
            render: (avatar) => avatarUser(avatar)
          },
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
          editable: false,
        },
        {
          title: 'Gender',
          dataIndex: 'gender',
          key: 'gender1',
          editable: false,
        },
        {
          title: 'BirthDate',
          dataIndex: 'birthDate',
          key: 'birthDate',
          editable: false,
        },
        {
            title: 'Address',
            dataIndex: 'address',
            key: 'address',
            editable: false,
          },
        {
            title: 'Onboard Date',
            dataIndex: 'onboardDate',
            key: 'onboardDate',
            editable: false,
        },
        {
            title: 'Department',
            dataIndex: 'department',
            key: 'department',
            editable: false,
        },
        {
            title: 'Level',
            dataIndex: 'level',
            key: 'level',
            editable: false,
        },
        {
            title: 'Onboard Status',
            dataIndex: 'onboardStatus',
            key: 'onboardStatus',
            align: 'center',
            render: (_, record) => 
                <>
                    <Button onClick={() => changeModal(record)}>{record.onboardStatus}</Button>
                </>
        },
        {
            title: 'Actions',
            dataIndex: 'actions',
            key: 'actions',
            align: 'center',
            render: (_, record) =>
                <Space size="middle">
                    <>
                    
                        { employees.length >= 1 ? (
                            <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record.key)}>
                                <div className="btn-delete"><DeleteOutlined /></div>
                            </Popconfirm>
                            ) : null 
                        }
                        {
                            <EditOutlined onClick={() => edit(record)}/>
                        }
                    
                    </>
                </Space>
                
        },
      ];
    const onSelectChange = (selectedRowKeys) => {
        setSelectedRowKeys({ selectedRowKeys });
    };
    const rowSelection = {
        selectedRowKeys,
        onChange: () => onSelectChange(),
    };
    

    const searchFilter = (filters) => {
        setIsLoading(true);
        
        if (filters.name === '' && filters.onboardStatus === '' && filters.birthDate === '' && filters.gender === '' && filters.level === '') {
            setEmployees(employeeArr);
            setIsLoading(false);
        } else {
            let dataEmp = [ ...employeeArr ];
            const dataResult = dataEmp.filter(item => {
                const filterdata = Object.keys(filters);
                const test = filterdata.every(propertyName => {
                    if (filters[propertyName] !== '' || filters[propertyName] !== undefined) {
                        if (String(item[propertyName]).toLowerCase().indexOf(filters[propertyName].toLowerCase())!== -1) {
                            return item;
                        }
                    }
                });
                return test;
            });
            setEmployees(dataResult);
            setIsLoading(false);
        }
    }

    return (
        <div className="employee-list">
            <div className="search" >
                <SearchComponent muiltiSearch={searchFilter} employeeList={props.employeeList}/>
            </div>
            <Spin tip="Loading..." spinning={isLoading}>
                <Table className="table"
                    rowSelection={rowSelection}
                    columns={columns}
                    dataSource={employees}
                    onRow={(record, rowIndex) => {
                        return {
                            onDoubleClick: () => edit(record), // click row
                        };
                    }}
                />
            </Spin>
            <ModalDefault modal={props.modal} hideModal={handleCancelModal} SubmitModal={handleOk}/>
            
        </div>
    )
    
}
const mapStateToProps = (state) => {
    return {
        employeeList: state.admin.employeeList,
        modal: state.admin.modal
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        handleChangeModal: (payload) => dispatch(changeModal(payload)),
        fetchAllEmployees: (employeeList) => dispatch(getEmployee(employeeList))
    };
  };
export default connect(mapStateToProps, mapDispatchToProps)(Employee);