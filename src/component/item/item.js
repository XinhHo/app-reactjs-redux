import './item.scss';
import { useState } from 'react';
function Item(props) {
    return (
        <div className="item">
              <input className="item-name" value={props.value}/>
              <button className="btn item-name-btn" onClick={() => props.removeItem()}>x</button>
        </div>
    );
}
export default Item;