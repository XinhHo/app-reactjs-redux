import './item.scss';
import { useState, useEffect } from 'react';
import { getListUser } from '../../../services/dataUsers';

function Item() {
    const [items, setItems] = useState([]);
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    useEffect(() => {
        let mounted = true;
        getListUser()
            .then(items => {
                if (mounted) {
                    console.log('items', items)
                    setIsLoaded(true)
                    setItems(items)
                }
            })
        return () => mounted = false;
    }, [])

    const getListItem = () => {
        const url = "http://localhost:3000/student";
        fetch(url, {
            "method": "GET"
        }).then(res => res.json())
            .then(
                (res) => {
                    console.log('res', res);
                    setIsLoaded(true);
                    setItems(res);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }, [])
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="card-cont">
                    { items.map(item => {
                        <div className="card-item" >
                            <img className="card__image" src={'https://thuthuatnhanh.com/wp-content/uploads/2019/08/avatar-cap-doi-de-thuong-nam.jpg'} alt="Person"></img>
                            <p className="card__name">Lily-Grace Colley</p>
                            <div className="grid-container">
                                <div className="grid-child-posts justify-content-start">156 Post</div>
                                <div className="grid-child-followers justify-content-end">1012 Likes</div>
                            </div>
                            <ul className="social-icons">
                                <li><a href="#"><i className="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i className="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i className="fa fa-codepen"></i></a></li>
                            </ul>
                            <button className="btn draw-border">Follow</button>
                            <button className="btn draw-border">Message</button>

                        </div>
                    })
                    }
                </div>

            );
        }

    }
    return (
        <div className="card-content">
            { items.map(item => (
                <div className="card-item" key={item.id}>
                    <img className="card__image" src={item.avatar} alt="Person"></img>
                    <p className="card__name">{item.firstName} {item.lastName}</p>
                    <div className="grid-container">
                        <div className="grid-child-posts justify-content-start">156 Post</div>
                        <div className="grid-child-followers justify-content-end">1012 Likes</div>
                    </div>
                    <ul className="social-icons">
                        <li><a href="#"><i className="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i className="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i className="fa fa-codepen"></i></a></li>
                    </ul>
                    <button className="btn draw-border">Follow</button>
                    <button className="btn draw-border">Message</button>

                </div>
            ))
            }

        </div>
    )
}
export default Item;