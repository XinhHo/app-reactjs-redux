import './todo-app.scss';
import ListTodo from '../list-todo/list-todo';
import AddTodo from '../add-todo/add-todo';
import { useState } from 'react';
function TodoApp(props) {
 return (
     <div className="app">
         <AddTodo />
         <ListTodo />
     </div>
 );
}
export default TodoApp;