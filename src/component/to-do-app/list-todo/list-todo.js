import './list-todo.scss';
import { connect } from 'react-redux';
import { getTodos } from '../../../redux/selectors';
import { removeTodo } from '../../../redux/actions';
import { useState, useEffect } from 'react';
import Item from '../item/item';
import { addNewUser } from '../../../services/dataUsers';

const mapStateToProps = state => ({
    todos: getTodos(state)
})

function TodoList(props) {
    const handleRemoveTodos = id => {
        props.removeTodo(id);
    }
    const [newItem, setNewItem] = useState({});
    const list = (todos) => {
        if (todos && todos.length > 0) {
            return (
                <div>
                    {todos.map((todo) => {
                        return (
                            <div className="item">
                                <input className="item-name" value={todo.content} key={todo.id}/>
                                <button className="btn item-name-btn" onClick={() => handleRemoveTodos(todo.id)}>x</button>
                            </div>
                        )
                    })
                        
                    }
                </div>
            )

        } else {
            return (<p>No data to show</p>)
        }
    }
    const items = () => {
        return (
            <div className="items">
                <Item></Item>
            </div>
        )
    }
    const handleChange = (newItem) => {
        debugger
        console.log('newItem', newItem)
        setNewItem(newItem)
      };
    const handleSubmit = (event) => {
        event.preventDefault();
        console.log('newItem', event)
        addNewUser(event)
      };
    const addItem = () => {
        return (
            <div> 
                <div className="add-user">
                    <label>First Name<input type="text" onChange={event => setNewItem({...newItem, firstName: event.target.value })} value={newItem.firstName} /></label>
                    <label>Last Name Item<input type="text" onChange={event => setNewItem({...newItem, lastName: event.target.value })} value={newItem.lastName} /></label>
                    <label>Avatar<input type="text" onChange={event => setNewItem({...newItem, avatar: event.target.value })} value={newItem.avatar} /></label>
                    <button type="submit" onClick={(event) => handleSubmit(event)}>Submit</button>
                </div>
                
            </div>
        )
    }
    
    return (
        <div>
            <div>{list(props.todos)}</div>
            <div>{addItem()}</div>
            <div className="container">{items()}</div>
        </div>
        // <div>{list(props.todos)}</div>
        // <div>
        //             {props && props.todos && props.todos.map((todo, index) => (
        //                 <div className="item">
        //                     <input className="item-name" value={todo.content} key={index}/>
        //                     <button className="btn item-name-btn">x</button>
        //                 </div>
        //             ))
                        
        //             }
        //         </div>
        
    );
}
export default connect(
    mapStateToProps, 
    {removeTodo}
    )(TodoList);