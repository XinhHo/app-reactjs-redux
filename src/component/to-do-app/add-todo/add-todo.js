import './add-todo.scss';
import { connect } from 'react-redux';
import { useState } from 'react';
import { addTodo } from '../../../redux/actions';
function AddTodo(props) {
    const [input, setInput] = useState('');
    const updateInput = input => {
        setInput(input);
    }
    const handleAddTodos = event => {
        
        props.addTodo(input);
        setInput('');
    }
    return(
        <div className="add-item">
            <input type="text" 
                className="add-name-label" 
                value={input}
                onChange={e => updateInput(e.target.value)}
            />
            <button className="bnt btn-primary add-name-btn" onClick={e => handleAddTodos(e)}>Submit</button>
        </div>
    );
}
export default connect(
    null,
    { addTodo }
)(AddTodo);