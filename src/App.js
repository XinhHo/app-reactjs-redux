import './App.scss';
import 'antd/dist/antd.css';
import React, {useState, useEffect} from "react";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Layout, Menu, Avatar, Dropdown} from 'antd';
import {
  UserOutlined ,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  ArrowRightOutlined,
  LogoutOutlined,
  SettingOutlined
} from '@ant-design/icons';
import AddItem from './component/add-item/add-item';
import Navbar from './component/cart-product/navbar/navbar';
import Products from './component/cart-product/products/products';
import Cart from './component/cart-product/cart/cart';
import Dashboard from './component/admin-page/admin/admin';
import Login from './component/admin-page/login/login';
import Logout from './component/admin-page/logout/logout';
import Employees from './component/admin-page/employee/index';
import EmployeesDetail from './component/admin-page/employee-detail/employee-detail';
import ProductAdmin from './component/admin-page/Products-admin/index';
import ProductDetail from './component/admin-page/Products-admin/edit-product/index';
import MenuSite from './common/layout-component/menu-site/menu';
const { Header, Content } = Layout;
function App() {
  const [pathName, setcurrentPath] = useState('');
  const [collapsed, setCollapsed] = useState(false);
  const [goEcommerce, setGoEcommerce] = useState(false);
  const toggle = () => {
    var onclickCollaps = !collapsed;
    setCollapsed(onclickCollaps);
};
const onChangePathName = (pathName) => {
  setcurrentPath(pathName);
}
const avatarUser = (avatar) => {
  if(avatar && avatar !== ''){
      return  (<Avatar src={avatar}/>)
  } else{
      return (<Avatar icon={<UserOutlined />}/>)
  }

}
const goToEcommerce = () => {
  console.log('hello');
  setGoEcommerce(true);
}

const menu = (
  <Menu>
    <Menu.Item key="0">
        <Link to="/homePage" onClick={e => goToEcommerce()}>
            <ArrowRightOutlined /> Ecommerce
        </Link>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="1">
        <Link to="" >
            <LogoutOutlined /> Logout
        </Link>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="3">
        <Link to="" >
            <SettingOutlined /> Settings
        </Link></Menu.Item>
</Menu>
);
const adminPage = () => {
  return (
    <Layout>
    <MenuSite changePathName={onChangePathName} collapsed={collapsed}/>
    <Layout className="site-layout">
      <Header className="site-layout-background">
        <div className="header-content">
          <div className="header-content-left">
              {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                className: 'trigger',
                onClick: () => toggle(),
              })}
          </div>
          <div className="header-content-right">
            <div className="avatar">
              <Dropdown overlay={menu} trigger={['click']}>
                <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                  { avatarUser()} Tieu Dao
                </a>
              </Dropdown>
               
            </div>

          </div>
        </div>
        
      </Header>
      <Content className="site-layout-background content">

        <div className="">
          <Switch>
              <Route exact path="/dashboard" component={Dashboard}></Route>
              <Route path="/products" component={ProductAdmin}></Route>
              <Route path="/product-detail/:id" component={ProductDetail}></Route>
              <Route path="/product-detail" component={ProductDetail}></Route>
              <Route path="/employee" component={Employees}></Route>
              <Route path="/employee-detail" component={EmployeesDetail}></Route>
              <Route path="/employee-detail/:id" component={EmployeesDetail}></Route>
          </Switch>
      </div>
      </Content>
    </Layout>
  </Layout>
  )
};
const EcommercePage = () => {
  return (
    <div>
      <Navbar />
      <Switch>
        <Route exact path="/homePage" component={Products}></Route>
        <Route path="/cart" component={Cart}></Route>
      </Switch>

    </div>
  )
}

  return (
    <Router>
      {
        goEcommerce ? EcommercePage() : adminPage()
      }
      
    </Router>
  );
}

export default App;
